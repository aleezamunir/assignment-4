﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.items.armor
{
    public class InvalidArmorException : Exception
    {
        public override string Message => "Can not equip this Armor";
    }
}

﻿using Assignment4.heroes.attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.items.armor
{
    public class Armor : Item
    {
        public Armor(string name, int requiredLevel, Slot slot, ArmorType type, HeroAttribute heroAttribute) :base(name, requiredLevel, slot)
        {
            Type = type;
            ArmorAttribute = heroAttribute;
        }

       

        public ArmorType Type { get; set; }
        public HeroAttribute ArmorAttribute { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.items.weapon
{
    public class Weapon: Item
    {
        public WeaponType Type { get; set; }
        public int WeaponDamage { get; set; }

        public Weapon(string name, int requiredLevel, Slot slot, int weaponDamage, WeaponType weaponType) : base(name, requiredLevel, slot)
        {
            WeaponDamage = weaponDamage;
            Type = weaponType;
        }
    }
}

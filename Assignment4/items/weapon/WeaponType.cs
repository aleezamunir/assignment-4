﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.items.weapon
{
    public enum WeaponType
    {
        Axes,
        Bows, 
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands
    }
}

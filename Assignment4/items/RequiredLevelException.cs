﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.items
{
    public class RequiredLevelException : Exception
    {
        public override string Message => "You need to be a higher level to equip this item";
    }
}

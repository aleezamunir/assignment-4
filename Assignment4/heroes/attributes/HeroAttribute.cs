﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.heroes.attributes
{ 
    public class HeroAttribute
    {
        public int Strength;
        public int Dexteriry;
        public int Intelligence;

        public HeroAttribute(int strength, int dexteriry, int intelligence)
        {
            Strength = strength;
            Dexteriry = dexteriry;
            Intelligence = intelligence;
        }

        public int ReturnSum(int num1, int num2)
        {
            return num1 + num2;
        }

        public int ReturnAll()
        {
            return Strength + Dexteriry + Intelligence;
        }

        public void updateAttributes(int StrenghtUpgrade, int DexteriryUpgrade, int IntelligenceUpgrade)
        {
            Strength += StrenghtUpgrade;
            Dexteriry += DexteriryUpgrade;
            Intelligence += IntelligenceUpgrade;
        }
    }
}

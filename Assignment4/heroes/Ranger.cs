﻿using Assignment4.items;
using Assignment4.items.armor;
using Assignment4.items.weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.heroes
{
    public class Ranger : Heroes
    {
        public Ranger(string name) : base(name)
        {
            Name = name;

            heroAttribute = new attributes.HeroAttribute(1, 7, 1);

            LevelAttributes = 9;

            ValidArmorTypes = new List<ArmorType>() { ArmorType.Mail, ArmorType.Leather };

            ValdiWeaponTypes = new List<WeaponType>() { WeaponType.Bows };
        }

        public override int Damage()
        {
            if (Equipment[Slot.Weapon] == null)
            {
                return 1 * (1 + heroAttribute.Dexteriry / 100);
            }
            else
            {
                Weapon weapon = (Weapon)Equipment[Slot.Weapon];
                return weapon.WeaponDamage * (1 + heroAttribute.Dexteriry / 100);
            }
        }

        public override void EquipArmor(Armor armor)
        {
            try
            {
                if(armor.RequiredLevel > Level)
                {
                    throw new RequiredLevelException();
                }
                try
                {
                    if(ValidArmorTypes.Contains(armor.Type))
                    {
                        Equipment[armor.Slot] = armor;
                        
                    } else
                    {
                        throw new InvalidArmorException();
                    }
                }
                catch (InvalidArmorException e)
                {
                    Console.WriteLine(e.Message);
                }
            } 
            catch (RequiredLevelException r)
            {
                Console.WriteLine(r.Message);
            }
        }

        public override void EquipWeapon(Weapon weapon)
        {
            try
            {
                if (weapon.RequiredLevel > Level)
                {
                    throw new RequiredLevelException();
                }
                try
                {
                    if (ValdiWeaponTypes.Contains(weapon.Type))
                    {
                        Equipment[Slot.Weapon] = weapon;
                    } else
                    {
                        throw new InvalidWeaponException();
                    } 
                    
                }
                catch (InvalidWeaponException e)
                {
                    Console.WriteLine(e.Message);

                }
            }
            catch (RequiredLevelException r)
            {
                Console.WriteLine(r.Message);
            }
        }

        public override void LevelUp()
        {
            this.Level += 1;

            this.LevelAttributes += 7;

            heroAttribute.updateAttributes(1, 5, 1);
        }
    }
}

﻿using Assignment4.heroes.attributes;
using Assignment4.items;
using Assignment4.items.armor;
using Assignment4.items.weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.heroes
{
    public abstract class Heroes
    {
        
        public string Name { get; set; }
        public int Level { get; set; }

        public HeroAttribute heroAttribute;

        public int LevelAttributes;

        public Dictionary<Slot, Item> Equipment;
        
        public List<WeaponType> ValdiWeaponTypes;
        public List<ArmorType> ValidArmorTypes;

        public Heroes(string name)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item>()
            {
                {Slot.Weapon, null! },
                {Slot.Body, null! },
                {Slot.Legs, null! },
                {Slot.Head, null! }
            };
        }

        public abstract void LevelUp();


        public abstract void EquipArmor(Armor armor);


        public abstract void EquipWeapon(Weapon weapon);


        public abstract int Damage();
        

        public int TotalAttributes()
        {
            int sum = 0;
            Armor body = (Armor)Equipment[Slot.Body];
            Armor legs = (Armor)Equipment[Slot.Legs];
            Armor head = (Armor)Equipment[Slot.Head];

            if(body != null)
            {
                sum += body.ArmorAttribute.ReturnAll();
            }

            if(legs != null)
            {
                sum += legs.ArmorAttribute.ReturnAll();
            }

            if(head != null)
            {
                sum += head.ArmorAttribute.ReturnAll();
            }

           
            return LevelAttributes + sum;
        }

        public string Display()
        {
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.Append(Name);
            stringbuilder.Append(Level);
            stringbuilder.Append(heroAttribute.Strength);
            stringbuilder.Append(heroAttribute.Dexteriry);
            stringbuilder.Append(heroAttribute.Intelligence);
            stringbuilder.Append(LevelAttributes);
            return stringbuilder.ToString();
        }
    }
}

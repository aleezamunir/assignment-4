# assignment-4


## RPGHeroes

## Description
Assignment number 4 where the task is to build a program that allows different heroes to be created. The heroes can equip items, such as Armor and Weapon, but there are some constraints to it. Each hero may have different attributes depending on the armor/weapon equiped and what level they are on. A total damage can be displayed, depending on the item equiped. 

The Project is built with different classes. Heroes have an abstract class, with 4 subclasses. Items is an abstract class, with two subclasses. There are three excpetion classes that will be thrown whenever a hero tries to equip an item they cannot equip. 

## Tech Used
Pure C# was used to make this project. The project was made using Visual Studio 2022

## Difficulities
I hade some difficulities with calculating the damage when equipping a new weapon/armor. It seems like the attributes from the previous item still remain in the heroes attributes section. 

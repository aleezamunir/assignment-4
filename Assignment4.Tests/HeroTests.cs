using Assignment4.heroes;
using Assignment4.heroes.attributes;
using Assignment4.items;
using Assignment4.items.armor;
using Assignment4.items.weapon;
using System.Xml.Linq;
using Xunit.Abstractions;
using static System.Net.Mime.MediaTypeNames;

namespace Assignment4.Tests
{
    public class HeroTests
    {
        [Fact]
        public void Check_MageName()
        {
            //Arrange
            string name = "Pluto";
            //Act
            Heroes mage = new Mage(name);
            //Assert 
            Assert.Equal(name, mage.Name);
        }

        [Fact]

        public void Check_MageLevel()
        {
            //Arrange
            string name = "Pluto";
            int expectedLevel = 1;
            //Act
            Heroes mage = new Mage(name);
            //Assert
            Assert.Equal(expectedLevel, mage.Level);
        }

        [Fact]

        public void Check_MageAttributes()
        {
            //Arrange
            string name = "Pluto";
            int[] expected = new int[] { 1, 1, 8 };
            //Act
            Heroes mage = new Mage(name);
            int[] mageAttributes = new int[] { mage.heroAttribute.Strength, mage.heroAttribute.Dexteriry, mage.heroAttribute.Intelligence };
            //Assert
            Assert.Equal(expected, mageAttributes);
        }

        [Fact]

        public void Increase_AttributesMage() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes mage = new Mage("name");
            
            int ExpectedLevelAttributes = mage.LevelAttributes + 7;

            //Act
            mage.LevelUp();
            int[] magehero = new int[] { mage.heroAttribute.Strength, mage.heroAttribute.Dexteriry, mage.heroAttribute.Intelligence };
            //Assert
            
            Assert.Equal(ExpectedLevelAttributes, mage.LevelAttributes);
        }
        [Fact]

        public void Increase_LevelMage() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes mage = new Mage("name");
            int ExpectedLevel = 2;
            
            //Act
            mage.LevelUp();
            
            //Assert
            Assert.Equal(ExpectedLevel, mage.Level);
            
            //Del dette i to
        }
        [Fact]

        public void CheckIf_HeroAttribute_Is_increasedMage() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes mage = new Mage("name");
            int[] ExpectedUpgrade = new int[] { 2, 2, 13 };
            //Act
            mage.LevelUp();
            int[] magehero = new int[] { mage.heroAttribute.Strength, mage.heroAttribute.Dexteriry, mage.heroAttribute.Intelligence };
            //Assert
            Assert.Equal(ExpectedUpgrade, magehero);
            //Del dette i to
        }

        [Fact]

        public void IncreaseAttributesRanger() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes ranger = new Ranger("name");
            
            int ExpectedLevelAttributes = ranger.LevelAttributes + 7;
            //Act
            ranger.LevelUp();
            //Assert
            
            Assert.Equal(ExpectedLevelAttributes, ranger.LevelAttributes);
            
        }
        [Fact]

        public void Increase_LevelRanger() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes ranger = new Ranger("name");
            int ExpectedLevel = 2;
            
            //Act
            ranger.LevelUp();
            //Assert
            Assert.Equal(ExpectedLevel, ranger.Level);
            
            //Del dette i to
        }
        [Fact]

        public void CheckIf_HeroAttribute_Is_increasedRanger() //M� lage en for hver hero sub klasse 
        {
            //Arrange 
            Heroes ranger = new Ranger("Ranger");
            int[] ExpectedUpgrade = new int[] { 2, 12, 2 };
            //Act
            ranger.LevelUp();
            int[] rangerUpgrade = new int[] { ranger.heroAttribute.Strength, ranger.heroAttribute.Dexteriry, ranger.heroAttribute.Intelligence };
            //Assert
            Assert.Equal(ExpectedUpgrade, rangerUpgrade);
        }
        [Fact]

        public void Increase_AttributesRogue() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes rogue = new Rogue("name");
            int ExpectedLevelAttributes = rogue.LevelAttributes + 6;
            //Act
            rogue.LevelUp();
            //Assert
            Assert.Equal(ExpectedLevelAttributes, rogue.LevelAttributes);
           
        }

        [Fact]

        public void Increase_LevelRogue() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes rogue = new Rogue("name");
            int ExpectedLevel = 2;
            //Act
            rogue.LevelUp();
            //Assert
            Assert.Equal(ExpectedLevel, rogue.Level);
            
        }
        [Fact]

        public void CheckIf_HeroAttribute_Is_increasedRogue() //M� lage en for hver hero sub klasse 
        {
            //Arrange 
            Heroes rogue = new Rogue("Ranger");
            int[] ExpectedUpgrade = new int[] { 3, 10, 2 };
            //Act
            rogue.LevelUp();
            int[] rogueUpgrade = new int[] { rogue.heroAttribute.Strength, rogue.heroAttribute.Dexteriry, rogue.heroAttribute.Intelligence };
            //Assert
            Assert.Equal(ExpectedUpgrade, rogueUpgrade);
        }

        [Fact]

        public void Increase_AttributesWarrior() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes warrior = new Warrior("name");
            int ExpectedLevelAttributes = warrior.LevelAttributes + 6;
            //Act
            warrior.LevelUp();
            //Assert

            Assert.Equal(ExpectedLevelAttributes, warrior.LevelAttributes);
            //Del dette i to
        }

        [Fact]

        public void Increase_LevelWarrior() //M� lage en for hver hero sub klasse 
        {
            //Arrange
            Heroes warrior = new Warrior("name");
            int ExpectedLevel = 2;

            //Act
            warrior.LevelUp();
            //Assert
            Assert.Equal(ExpectedLevel, warrior.Level);

            //Del dette i to
        }
        [Fact]

        public void CheckIf_HeroAttribute_Is_increasedWarrior()
        {
            //Arrange 
            Heroes warrior = new Warrior("Ranger");
            int[] ExpectedUpgrade = new int[] { 8, 4, 2 };
            //Act
            warrior.LevelUp();
            int[] warriorUpgrade = new int[] { warrior.heroAttribute.Strength, warrior.heroAttribute.Dexteriry, warrior.heroAttribute.Intelligence };
            //Assert
            Assert.Equal(ExpectedUpgrade, warriorUpgrade);
        }

        [Fact]

        public void Check_If_weapon_Name_is_correct()
        {
            //Arrange
            string name = "Common Axe";
            //Act
            Item weapon = new Weapon(name, 1, Slot.Weapon, 2, WeaponType.Axes);
            //Assert
            Assert.Equal(name, weapon.Name);
        }

        [Fact]

        public void Check_If_Weapon_Level_Is_Correct()
        {
            //Arrange
            int requiredLevel = 1;
            //Act
            Item weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            //Assert
            Assert.Equal(requiredLevel, weapon.RequiredLevel);
        }

        [Fact]

        public void Check_If_Weapon_Slot_Is_Correct()
        {
            //Arrange
            Slot slot = Slot.Weapon;
            //Act
            Item weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            //Assert
            Assert.Equal(slot, weapon.Slot);
        }

        [Fact]

        public void Check_If_Weapon_Type_Is_Correct()
        {
            //Arrange
            WeaponType type = WeaponType.Axes;
            //Act
            Weapon weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            //Assert
            Assert.Equal(type, weapon.Type);
        }

        [Fact]

        public void Check_If_Weapon_Damage_Is_Correct()
        {
            //Arrange
            int Damage = 2;
            //Act
            Weapon weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            //Assert
            Assert.Equal(Damage, weapon.WeaponDamage);
        }
        [Fact]

        public void Check_If_Armor_Name_is_correct()
        {
            //Arrange
            string name = "Common Plate Chest";
            //Act
            Item armor = new Armor("Common Plate Chest", 1, Slot.Body, ArmorType.Plate, new heroes.attributes.HeroAttribute(1, 0, 0));
            //Assert
            Assert.Equal(name, armor.Name);
        }

        [Fact]

        public void Check_If_Armor_Level_Is_Correct()
        {
            //Arrange
            int requiredLevel = 1;
            //Act
            Item armor = new Armor("Common Plate Chest", 1, Slot.Body, ArmorType.Plate, new heroes.attributes.HeroAttribute(1, 0, 0));
            //Assert
            Assert.Equal(requiredLevel, armor.RequiredLevel);
        }

        [Fact]

        public void Check_If_Armor_Slot_Is_Correct()
        {
            //Arrange
            Slot slot = Slot.Body;
            //Act
            Item armor = new Armor("Common Plate Chest", 1, Slot.Body, ArmorType.Plate, new heroes.attributes.HeroAttribute(1, 0, 0));
            //Assert
            Assert.Equal(slot, armor.Slot);
        }

        [Fact]
        public void Check_If_Armor_Type_Is_Correct()
        {
            //Arrange
            ArmorType type = ArmorType.Plate;
            //Act
            Armor armor = new Armor("Common Plate Chest", 1, Slot.Body, ArmorType.Plate, new heroes.attributes.HeroAttribute(1, 0, 0));
            //Assert
            Assert.Equal(type, armor.Type);
        }

        [Fact]

        public void Check_If_Armor_Attributes_Is_correct()
        {
            //Arrange
            int[] expected = new int[] { 1, 0, 0 };
            Armor armor = new Armor("Common Plate Chest", 1, Slot.Body, ArmorType.Plate, new heroes.attributes.HeroAttribute(1, 0, 0));
            //Act
            int[] result = new int[] { armor.ArmorAttribute.Strength, armor.ArmorAttribute.Dexteriry, armor.ArmorAttribute.Intelligence };
            //Assert 
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Hero_Should_Equip_Weapon()//Endre metoden slik at den sjekker for requiredlevel
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            Item weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            //Act
            hero.EquipWeapon((Weapon)weapon);
            Assert.Equal(weapon, hero.Equipment[Slot.Weapon]);
        }

        [Fact]
        public void Hero_Should_Not_Equip_Weapon2()//Endre metoden slik at den sjekker for requiredlevel
        {
            //Arrange
            Heroes hero = new Mage("Stewart");
            Item weapon = new Weapon("Common Axe", 2, Slot.Weapon, 2, WeaponType.Wands);
            //Act
            hero.EquipWeapon((Weapon)weapon);
            Assert.Null(hero.Equipment[Slot.Weapon]);
        }

        [Fact]
        public void Hero_Should_Not_Equip_Weapon()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            Item weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Bows);
            //Act
            hero.EquipWeapon((Weapon)weapon);
            Assert.Null(hero.Equipment[Slot.Weapon]);
        }

        [Fact]
        public void Hero_Should_Equip_Armor()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            HeroAttribute armorAttribute = new HeroAttribute(1, 0, 0);
            string Name = "Common Plate Chest";
            int RequiredLevel = 1;
            Slot slot = Slot.Body;
            ArmorType type = ArmorType.Plate;
            Item armor = new Armor(Name, RequiredLevel, slot, type, armorAttribute);
            //Act
            hero.EquipArmor((Armor)armor);
            Assert.Equal(armor, hero.Equipment[slot]);
        }

        [Fact]
        public void Hero_Should_Not_Equip_Armor()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            HeroAttribute armorAttribute = new HeroAttribute(1, 0, 0);
            string Name = "Common Plate Chest";
            int RequiredLevel = 2;
            Slot slot = Slot.Body;
            ArmorType type = ArmorType.Plate;
            Item armor = new Armor(Name, RequiredLevel, slot, type, armorAttribute);
            //Act
            hero.EquipArmor((Armor)armor);
            Assert.Null(hero.Equipment[slot]);
        }

        [Fact]
        public void Hero_Should_Not_Equip_Armor2()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            HeroAttribute armorAttribute = new HeroAttribute(1, 0, 0);
            string Name = "Common Plate Chest";
            int RequiredLevel = 1;
            Slot slot = Slot.Body;
            ArmorType type = ArmorType.Mail;
            Item armor = new Armor(Name, RequiredLevel, slot, type, armorAttribute);
            //Act
            hero.EquipArmor((Armor)armor);
            //Assert
            Assert.Null(hero.Equipment[Slot.Weapon]);
        }

        [Fact]

        public void Calculate_Total_Attributes_With_No_Equipment()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            int expected = 8;
            //Act
            int result = hero.TotalAttributes();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Calculate_Total_attributes_With_No_Equipment_LevelUp()
        {
            //Arrange 
            Heroes hero = new Warrior("Stewart");
            int expected = 14;
            //Act
            hero.LevelUp();
            int result = hero.TotalAttributes();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public void Calculate_Total_attributes_With_One_Armor()
        {
            //Arrange 
            Heroes hero = new Warrior("Stewart");
            int expected = 9;

            HeroAttribute armorAttribute = new HeroAttribute(1, 0, 0);
            string Name = "Common Plate Chest";
            int RequiredLevel = 1;
            Slot slot = Slot.Body;
            ArmorType type = ArmorType.Plate;
            Item armor = new Armor(Name, RequiredLevel, slot, type, armorAttribute);
            //Act
            hero.EquipArmor((Armor)armor);
            int result = hero.TotalAttributes();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]

        public void Calculate_Total_Attributes_With_Two_Armor()
        {
            //Arrange 
            Heroes hero = new Warrior("Stewart");
            int expected = 17;

            HeroAttribute armorAttribute = new HeroAttribute(1, 0, 0);
            string Name = "Common Plate Chest";
            int RequiredLevel = 1;
            Slot slot = Slot.Body;
            ArmorType type = ArmorType.Plate;
            Item armor = new Armor(Name, RequiredLevel, slot, type, armorAttribute);

            HeroAttribute armorAttribute2 = new HeroAttribute(1, 4, 3);
            string Name2 = "Common Mail";
            Slot slot2 = Slot.Legs;
            ArmorType type2 = ArmorType.Mail;  
            Item armor2 = new Armor(Name2, RequiredLevel, slot2, type2, armorAttribute2);
            //Act
            hero.EquipArmor((Armor)armor2);
            hero.EquipArmor((Armor)armor);
            int result = hero.TotalAttributes();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]

        public void Calculate_Total_Attributes_With_ReplacedArmor()
        {
            //Arrange 
            Heroes hero = new Warrior("Stewart");
            int expected = 16;

            HeroAttribute armorAttribute = new HeroAttribute(1, 0, 0);
            string Name = "Common Plate Chest";
            int RequiredLevel = 1;
            Slot slot = Slot.Body;
            ArmorType type = ArmorType.Plate;
            Item armor = new Armor(Name, RequiredLevel, slot, type, armorAttribute);

            HeroAttribute armorAttribute2 = new HeroAttribute(1, 4, 3);
            string Name2 = "Common Mail";
            Slot slot2 = Slot.Body;
            ArmorType type2 = ArmorType.Mail;
            Item armor2 = new Armor(Name2, RequiredLevel, slot2, type2, armorAttribute2);
            //Act
            hero.EquipArmor((Armor)armor);
            hero.EquipArmor((Armor)armor2);
            int result = hero.TotalAttributes();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]

        public void Calculate_Damage_With_No_Equipment()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            int expected = 1;
            //Act
            int result = hero.Damage();
            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]

        public void Calculate_Damage_with_Weapon()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            Item weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            hero.EquipWeapon((Weapon)weapon);
            int expected = 2;
            //Act
            int result = hero.Damage();
            //Assert 
            Assert.Equal(expected, result);
        }

        [Fact]

        public void Calculate_Damage_With_Replaced_Weapon()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            Item weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            hero.EquipWeapon((Weapon)weapon);
            Item newWeapon = new Weapon("Common Plate", 1, Slot.Weapon, 3, WeaponType.Axes);
            int expected = 3;
            //Act
            hero.EquipWeapon((Weapon)newWeapon);
            int result = hero.Damage();
            //Assert 
            Assert.Equal(expected, result); 
        }

        [Fact]

        public void Calculate_Damage_With_Weapon_And_Armor()
        {
            //Arrange
            Heroes hero = new Warrior("Stewart");
            Item weapon = new Weapon("Common Axe", 1, Slot.Weapon, 2, WeaponType.Axes);
            hero.EquipWeapon((Weapon)weapon);

            HeroAttribute armorAttribute = new HeroAttribute(1, 0, 0);
            string Name = "Common Plate Chest";
            int RequiredLevel = 1;
            Slot slot = Slot.Body;
            ArmorType type = ArmorType.Plate;
            Item armor = new Armor(Name, RequiredLevel, slot, type, armorAttribute);
            hero.EquipArmor((Armor)armor);

            int expected = 2;
            //Act
            
            int result = hero.Damage();
            //Assert 
            Assert.Equal(expected, result);
        }
    }
}
